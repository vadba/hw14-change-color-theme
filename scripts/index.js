const theme = document.querySelector('span.theme');

if (!!localStorage.getItem('theme')) {
    theme.innerText = 'White Theme';
    document.body.classList.add('pink');
} else {
    theme.innerText = 'Pink Theme';
}

theme.addEventListener('click', e => {
    if (!!localStorage.getItem('theme')) {
        localStorage.removeItem('theme');
        document.body.classList.toggle('pink');
        theme.innerText = 'Pink Theme';
    } else {
        localStorage.setItem('theme', true);
        document.body.classList.toggle('pink');
        theme.innerText = 'White Theme';
    }
});
